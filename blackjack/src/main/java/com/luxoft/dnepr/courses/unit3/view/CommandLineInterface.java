package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class CommandLineInterface {
    private static final String STATE_WIN = "Congrats! You win!";
    private static final String STATE_LOOSE = "Push. Everybody has equal amount of points.";
    private static final String STATE_PUSH = "Sorry, today is not your day. You loose.";

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: Julia Olinovich\n" +
                "(C) Luxoft 2013\n");
        GameController controller = GameController.getInstance();

        controller.newGame();

        output.println();
        printState(controller);

        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see com.luxoft.dnepr.courses.unit3.view.CommandLineInterfaceTest
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */
    private boolean execute(String command, GameController controller) {
        switch (command.toUpperCase()) {
            case Command.HELP:
                printHelp();
                return true;
            case Command.MORE:
                return playerMore(controller);
            case Command.STOP:
                return playerStop(controller);
            case Command.EXIT:
                output.println("Bye! Good luck!");
                return false;
            default:
                output.println("Invalid command");
                return true;
        }
    }

    private void printHelp() {
        output.print("Usage: \n" +
                "\thelp - prints this message\n" +
                "\thit - requests one more card\n" +
                "\tstand - I'm done - lets finish\n" +
                "\texit - exits game\n");
    }

    private boolean playerMore(GameController controller) {
       printState(controller);
        if (!controller.requestMore()) {
            output.println();
            output.print(STATE_LOOSE);
            return false;
        }
        return true;
    }

   /* private boolean playerStop1(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:\n");
        printState(controller);
        output.println("");
        output.print(controller.getWinState().getDescription());
        return  false;
    }*/

    private boolean playerStop(GameController controller) {
        controller.requestStop();
        output.println("Dealer turn:\n");
        printState(controller);
        switch (controller.getWinState()) {
            case WIN:
                output.println("\n  " + STATE_WIN);
                break;
            default:
                output.println("\n  " + STATE_PUSH);
                break;
            case LOOSE:
                output.println("\n  " + STATE_LOOSE);
                break;
        }
        return  true;
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        format(myHand);
        List<Card> dealersHand = controller.getDealersHand();
        format(dealersHand);
    }

    private void format(List<Card> hand) {
        int sumCost = 0;
        for (Card card : hand) {
            output.print(card.getRank().getName() + " ");
            sumCost += card.getCost();
        }
        output.println("(total " + sumCost + ")");
    }
}
