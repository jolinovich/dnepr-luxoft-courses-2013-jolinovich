package com.luxoft.dnepr.courses.unit3.web;

import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;

public class MethodDispatcher {

    public Response dispatch(Request request, Response response) {
        String method = request.getParameters().get("method");
        if (method == null) {
            return null;
        }
        if (method.equals("requestMore")) {
            return requestMore(response);
        } else if (method.equals("requestStop")) {
            return requestStop(response);
        } else if (method.equals("winState")) {
            return winState(response);
        } else if (method.equals("newGame")) {
            return newGame(response);
        }
        return null;
    }

    private Response requestMore(Response response) {
        response.write("{ result: " + GameController.getInstance().requestMore());
        response.write(", myhand: ");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write("}");
        return response;
    }

    private Response requestStop(Response response) {
        GameController.getInstance().requestStop();
        response.write("{\"dealershand\": ");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");
        return response;
    }

    private Response winState(Response response) {
        response.write("{ \"result\": \"" + GameController.getInstance().getWinState() + "\"}");
        return response;
    }

    private Response newGame(Response response) {
        GameController.getInstance().newGame();
        response.write("{ \"myhand\": ");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write(",\"dealershand\": ");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");
        return response;
    }

    private void writeHand(Response response, List<Card> hand) {
        boolean isFirst = true;
        response.write("[");
        for (Card card : hand) {
            if (isFirst) {
                isFirst = false;
            } else {
                response.write(",");
            }
            response.write("{rank: '");
            response.write(card.getRank().getName());
            response.write("', suit: '");
            response.write(card.getSuit().name());
            response.write("', cost: ");
            response.write(String.valueOf(card.getCost()));
            response.write("}");
        }
        response.write("]");
    }

}
