package com.luxoft.dnepr.courses.unit3.controller;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class GameController {
    private static GameController controller;
    private static final int MAX_BLACKJACK_SCORE = 21;
    private static final int MIN_DEALER_SCORE = 17;
    public List<Card> playerHand;
    public List<Card> dealerHand;
    public List<Card> deck;

    private GameController() {
        // инициализация переменных тут
        playerHand = new LinkedList<Card>();
        dealerHand = new LinkedList<Card>();
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }
        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {
        deck = Deck.createDeck(1);
        dealerHand.clear();
        playerHand.clear();
        shuffler.shuffle(deck);
        for (int i = 0; i < 3; i++) {
            Card gameCard = deck.get(0);
            deck.remove(0);
            if (i < 2) {
                playerHand.add(gameCard);
            } else {
                dealerHand.add(gameCard);
            }
        }
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        if (Deck.costOf(playerHand) < MAX_BLACKJACK_SCORE && !deck.isEmpty()) {
            Card gameCard = deck.get(0);
            deck.remove(0);
            playerHand.add(gameCard);
            return true;
        }
        return false;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        while (Deck.costOf(dealerHand) < MIN_DEALER_SCORE && !deck.isEmpty()) {
            Card gameCard = deck.get(0);
            deck.remove(0);
            dealerHand.add(gameCard);
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        if (Deck.costOf(playerHand) > MAX_BLACKJACK_SCORE)
            return WinState.LOOSE;
        if (Deck.costOf(dealerHand) > MAX_BLACKJACK_SCORE)
            return WinState.WIN;
        if (Deck.costOf(playerHand) < Deck.costOf(dealerHand))
            return WinState.LOOSE;
        if (Deck.costOf(playerHand) > Deck.costOf(dealerHand))
            return WinState.WIN;
        return WinState.PUSH;
    }

    // Возвращаем руку игрока
    public List<Card> getMyHand() {
        return playerHand;
    }

    //Возвращаем руку диллера
    public List<Card> getDealersHand() {
        return dealerHand;
    }
}
