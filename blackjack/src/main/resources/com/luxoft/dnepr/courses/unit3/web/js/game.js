$(document).ready(function () {
    $("#btNewGame").click(function () {
        $("#btHit").click(btHit);
        $("#btStop").click(btStop);
        $("#dhand div").remove();
        $("#phand div").remove();
        $("div.cur_dhand").remove();
        $("div.cur_phand").remove();
        $(".msg").html('');
        $.getJSON("service?method=newGame", function (json) {
            $.each(json.myhand, function () {
                $d = $("<div>");
                $d.addClass("cur_phand").appendTo("#phand");
                $("<img>").appendTo($d).attr('src', 'img/' + this['rank']+ this['suit'].charAt(0) + '.png');
            });
            $.each(json.dealershand, function () {
                $d = $("<div>");
                $d.addClass("cur_dhand").appendTo("#dhand");
                $("<img>").appendTo($d).attr('src', 'img/' + this['rank']+ this['suit'].charAt(0) + '.png');
            });
        });
    });
});

function btHit() {
    $("#phand div").remove();
    $.getJSON("service?method=requestMore", function (json) {
        $.each(json.myhand, function () {
            $d = $("<div>");
            $d.addClass("cur_phand").appendTo("#phand");
            $("<img>").appendTo($d).attr('src', 'img/' + this['rank']+ this['suit'].charAt(0) + '.png');
        });
    $.getJSON("service?method=winState", function (json) {
        if (json.result === "LOOSE") {
            $(".msg").html('Sorry. You loose');
            $("#btHit").unbind('click');
            $("#btStop").unbind('click');
        }
    });
});
}

function btStop() {
    $("#dhand div").remove();
    $.getJSON(method, function (json) {
        $.each(json.dealershand, function () {
            $d = $("<div>");
            $d.addClass("cur_dhand").appendTo("#dhand");
            $("<img>").appendTo($d).attr('src', 'img/' + this['rank']+ this['suit'].charAt(0) + '.png');
         });
     });
    $.getJSON("service?method=winState", function (json) {
            if (json.result === "LOOSE") {
                $(".msg").html("Sorry. You loose");
            } else {
                $(".msg").html("Congrats! You win!");
            }
        });
        $("#btHit").unbind('click');
        $("#btStop").unbind('click');
}
