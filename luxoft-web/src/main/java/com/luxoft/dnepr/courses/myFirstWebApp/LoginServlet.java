package com.luxoft.dnepr.courses.myFirstWebApp;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LoginServlet extends HttpServlet implements ServletContextListener {
    private static Map<String, String> map = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().write("login");
        response.sendRedirect("index.html");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();
        session.invalidate();
        String userName = request.getParameter("login");
        String password = request.getParameter("password");
        response.getWriter().write(userName);
        response.getWriter().write(password);
        if (userName == null || password == null) {
            response.sendRedirect("index.html");
        }
        if (map.get(userName)!=null && map.get(userName).equals(password)) {
            request.getSession().setAttribute("userName", request.getParameter("userName"));
            request.getRequestDispatcher("user").forward(request, response);
        } else {
            request.setAttribute("err", "Wrong login or password");
            request.setAttribute("login", userName);
            request.setAttribute("password", password);
            request.getRequestDispatcher("index.html").forward(request, response);
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String dir = servletContextEvent.getServletContext().getRealPath("/");
        String xmlName = servletContextEvent.getServletContext().getInitParameter("users");
        File xmlFile = new File(dir + "META-INF\\" + xmlName);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFile);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getDocumentElement().getChildNodes();
            String name, password;
            for (int tmp = 0; tmp < nodeList.getLength(); tmp++) {
                Node node = nodeList.item(tmp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    NamedNodeMap attrs = node.getAttributes();
                    name = attrs.item(0).getNodeValue();
                    password = attrs.item(1).getNodeValue();
                    map.put(name, password);
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}


