package com.luxoft.dnepr.courses.myFirstWebApp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class DummyServlet extends HttpServlet {
    private Map<String, String> storage = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
            throws IOException,ServletException{
        this.doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setContentType("application/json; charset=utf-8");
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        if (age == null || name == null) {
            response.setStatus(500);
            writer.println("{\"error\": \"Illegal parameters\"}");
        } else if (storage.containsKey(name)) {
            response.setStatus(500);
            writer.println("{\"error\": \"Name " + name + " already exists\"}");
        } else {
            response.setStatus(201);
            storage.put(name, age);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setContentType("application/json; charset=utf-8");
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        if (age == null || name == null) {
            response.setStatus(500);
            writer.println("{\"error\": \"Illegal parameters\"}");
        } else if (!storage.containsKey(name)) {
            response.setStatus(500);
            writer.println("{\"error\": \"Name " + name + " does not exist\"}");
        } else {
            response.setStatus(202);
            storage.put(name, age);
        }
    }
}
