package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    private void checkIdIsNull(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Illegal argument in Wallet: id is NULL");
        }
    }

    private void checkAmountIsNull(BigDecimal amount) {
        if (amount == null) {
            throw new IllegalArgumentException("Illegal argument in Wallet: amount is NULL");
        }
    }

    private void checkStatusIsNull(WalletStatus status) {
        if (status == null) {
            throw new IllegalArgumentException("Illegal argument in Wallet: status is NULL");
        }
    }

    private void checkMaxAmountIsNull(BigDecimal maxAmount) {
        if (maxAmount == null) {
            throw new IllegalArgumentException("Illegal argument in Wallet: maxAmount is NULL");
        }
    }

    public Wallet() {

    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        checkIdIsNull(id);
        checkAmountIsNull(amount);
        checkStatusIsNull(status);
        checkMaxAmountIsNull(maxAmount);
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        checkIdIsNull(id);
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        checkAmountIsNull(amount);
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        checkStatusIsNull(status);
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        checkMaxAmountIsNull(maxAmount);
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (amountToWithdraw == null) {
            throw new IllegalArgumentException("Illegal argument in Wallet: amountToWithdraw is NULL");
        }
        walletIsBlocked();
        if (amountToWithdraw.compareTo(amount) > 0) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "has insufficient funds ("
                    + amount.setScale(2) + " < " + amountToWithdraw.setScale(2) + ")");
        }
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        amount = amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (amountToTransfer == null) {
            throw new IllegalArgumentException("Illegal argument in Wallet: amountToTransfer is NULL");
        }
        walletIsBlocked();
        BigDecimal sumAfterTransfer = amountToTransfer.add(amount);
        if (sumAfterTransfer.compareTo(maxAmount) > 0) {
            throw new LimitExceededException(id, amountToTransfer.setScale(2), amount.setScale(2), "wallet limit exceeded ("
                    + amount.setScale(2) + " + " + amountToTransfer.setScale(2) + " > " + maxAmount.setScale(2) + ")");
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        amount = amount.add(amountToTransfer);
    }

    private void walletIsBlocked() throws WalletIsBlockedException {
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, "wallet is blocked");
        }
    }
}
