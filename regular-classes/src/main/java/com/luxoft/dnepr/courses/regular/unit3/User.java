package com.luxoft.dnepr.courses.regular.unit3;

public class User implements UserInterface {
    private Long id;
    private String name;
    private WalletInterface wallet;

    private void checkIdIsNull(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Illegal argument in User: id is NULL");
        }
    }

    private void checkNameIsNull(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Illegal argument in User: name is NULL");
        }
    }

    private void checkWalletIsNull(WalletInterface wallet) {
        if (wallet == null) {
            throw new IllegalArgumentException("Illegal argument in User: wallet is NULL");
        }
    }

    public User() {

    }

    public User(long id, String name, WalletInterface wallet) {
        checkIdIsNull(id);
        checkNameIsNull(name);
        checkWalletIsNull(wallet);
        this.id = id;
        this.name = name;
        this.wallet = wallet;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        checkIdIsNull(id);
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        checkNameIsNull(name);
        this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        checkWalletIsNull(wallet);
        this.wallet = wallet;
    }
}
