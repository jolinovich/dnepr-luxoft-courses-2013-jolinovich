package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankInterface {
    private Map<Long, UserInterface> users = new HashMap<>();

    public Bank() {

    }

    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError {
        String actualJavaVersion = System.getProperty("java.version");
        if (!actualJavaVersion.equals(expectedJavaVersion)) {
            String errorMessage = "Illegal Java version. Actual: " + actualJavaVersion;
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, errorMessage);
        }
        this.users = new HashMap<Long, UserInterface>();
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        if (users == null) {
            throw new IllegalArgumentException("Illegal argument in Bank: users are NULL");
        }
        this.users = users;
    }

    private void checkAmountIsNull(BigDecimal amount) {
        if (amount == null) {
            throw new IllegalArgumentException("Illegal argument in Bank: amount is NULL");
        }
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        checkAmountIsNull(amount);
        UserInterface fromUser = getUserId(fromUserId);
        UserInterface toUser = getUserId(toUserId);
        WalletInterface fromWallet = getFromWallet(fromUser, amount);
        WalletInterface toWallet = getToWallet(toUser, amount);
        fromWallet.withdraw(amount);
        toWallet.transfer(amount);
    }

    private UserInterface getUserId(Long userId) throws NoUserFoundException {
        UserInterface user = users.get(userId);
        if (user == null) {
            throw new NoUserFoundException(userId, "User with id " + userId + " not found");
        }
        return user;
    }

    private WalletInterface getFromWallet(UserInterface user, BigDecimal amount) throws TransactionException {
        WalletInterface wallet = user.getWallet();
        try {
            wallet.checkWithdrawal(amount);
        } catch (Exception ex) {
            throw new TransactionException("User '" + user.getName() + "' " + ex.getMessage());
        }
        return wallet;
    }

    private WalletInterface getToWallet(UserInterface user, BigDecimal amount) throws TransactionException {
        WalletInterface wallet = user.getWallet();
        try {
            wallet.checkTransfer(amount);
        } catch (Exception ex) {
            throw new TransactionException("User '" + user.getName() + "' " + ex.getMessage());
        }
        return wallet;
    }
}
