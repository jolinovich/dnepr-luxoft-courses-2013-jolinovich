package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct {//implements Product {
    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        if (!super.equals(obj)) return false;
        // object must be Book at this point
        Book book = (Book) obj;
        return publicationDate == book.publicationDate || (publicationDate != null && publicationDate.equals(book.publicationDate));
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 31 * hash + (null == publicationDate ? 0 : publicationDate.hashCode());
        return hash;
    }

    public Book clone() throws CloneNotSupportedException {
        Book bookClone = (Book) super.clone();
        bookClone.publicationDate = (Date) publicationDate.clone();
        return bookClone;
    }

}
