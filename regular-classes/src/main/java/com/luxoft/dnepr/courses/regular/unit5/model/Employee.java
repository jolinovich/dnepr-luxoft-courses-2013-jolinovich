package com.luxoft.dnepr.courses.regular.unit5.model;

public class Employee extends Entity {
    private int salary;

    public Employee() {
    }

    public Employee(long id, int salary) {
        super(id);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}