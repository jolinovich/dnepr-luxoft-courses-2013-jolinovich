package com.luxoft.dnepr.courses.regular.unit14;

import java.io.*;
import java.util.HashSet;

public class Vocabulary {
    private static HashSet<String> vocabulary = new HashSet<>();

    private String readVocabulary(String filename) {
        try (Reader reader = new InputStreamReader(new FileInputStream(filename))) {
            StringBuilder builder = new StringBuilder();
            int letter = reader.read();
            while (letter > 0) {
                builder.append((char) letter);
                letter = reader.read();
            }
            return builder.toString();
        }  catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vocabulary()  {
        String vocabularyFileURL = getClass().getResource("sonnets.txt").getFile();
        String[] words = readVocabulary(vocabularyFileURL).split("[ ,.;:?!\n]");
        for (String str : words) {
            if (str.length() > 3) {
                vocabulary.add(str.toLowerCase());
            }
        }
    }

    public String randomWord() {
        return (String) vocabulary.toArray()[(int) (Math.random() * vocabulary.size())];
    }

    public int getSize() {
        return vocabulary.size();
    }
}
