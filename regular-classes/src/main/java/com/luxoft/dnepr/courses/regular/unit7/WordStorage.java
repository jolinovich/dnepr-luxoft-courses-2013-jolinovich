package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class WordStorage {

    private Map<String, Integer> storage = new HashMap<>();

    public void save(String word) {
        int countWord = 0;
        synchronized (this) {
            if (storage.containsKey(word)) {
                countWord = storage.get(word);
            }
            storage.put(word, countWord + 1);
        }
    }

    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(storage);
    }
}
