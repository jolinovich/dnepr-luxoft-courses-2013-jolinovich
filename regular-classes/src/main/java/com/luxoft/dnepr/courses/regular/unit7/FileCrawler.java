package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class FileCrawler {

    private final WordStorage wordStorage = new WordStorage();
    private final int maxNumberOfThreads;
    private final String rootFolder;

    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    public FileCrawlerResults execute() {
        BlockingQueue<File> fileQueue = new LinkedBlockingQueue<>();
        List<File> processedFiles = new ArrayList<>();
        if (maxNumberOfThreads > 1) {
            new Thread(new Producer(rootFolder, fileQueue)).start();
            for (int i = 1; i < maxNumberOfThreads; i++) {
                new Thread(new Consumer(wordStorage, fileQueue)).start();
            }
        } else {
            new Producer(rootFolder, fileQueue).run();
            new Consumer(wordStorage, fileQueue).run();
        }
        return new FileCrawlerResults(processedFiles, wordStorage.getWordStatistics());
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }
}
