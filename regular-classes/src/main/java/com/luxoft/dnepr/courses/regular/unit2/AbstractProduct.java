package com.luxoft.dnepr.courses.regular.unit2;

public abstract class AbstractProduct implements Product, Cloneable {
    private String code;
    private String name;
    private double price;

    public AbstractProduct(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        if (this == obj) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        // object must be AbstractProduct at this point
        AbstractProduct abstractProduct = (AbstractProduct) obj;
        if (code != null ? !code.equals(abstractProduct.code) : abstractProduct.code != null) return false;
        if (name != null ? !name.equals(abstractProduct.name) : abstractProduct.name != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        //return super.hashCode();
        int hash = 7;
        hash = 31 * hash + (null == code ? 0 : code.hashCode());
        hash = 31 * hash + (null == name ? 0 : name.hashCode());
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
