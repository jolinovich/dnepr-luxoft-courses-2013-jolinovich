package com.luxoft.dnepr.courses.unit2;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }


    private static int wordLength(String str) {
        int countLetter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.toUpperCase().charAt(i) >= 'A' && str.toUpperCase().charAt(i) <= 'Z')
                countLetter++;
        }
        return countLetter;
    }

    //Function, which returns average length of words in string.
    public static double wordAverageLength(String str) {
        str = str.trim();
        String[] strArray = str.split(" ");
        int countWords = 0;
        int countLetter = 0;
        for (int i = 0; i < strArray.length; i++) {
            if (strArray[i].length() > 0) {
                countWords++;
                countLetter += wordLength(strArray[i]);
            }
        }
        if (countWords > 0) {
            return (double) (countLetter) / countWords;
        } else {
            return 0;
        }

    }

    //Function, which joins strings with a separator
    private static String joinStrings(String[] strArray, String separator) {
        String strNew = strArray[0];
        for (int i = 1; i < strArray.length; i++) {
            strNew = strNew + separator + strArray[i];
        }
        return strNew;
    }

    //Function, which reverse string
    private static String reverseString(String str) {
        String strTemp = "";
        for (int i = 0; i < str.length(); i++) {
            strTemp = str.charAt(i) + strTemp;
        }
        return strTemp;
    }

    //Function, which reverse every word in string
    public static String reverseWords(String str) {
        if (str.length() == 0)
            return "";
        int index = str.length() - 1;
        while (str.charAt(index) == ' ') {
            index--;                             //number of the last character other than space
        }
        String[] strTempArray = str.split(" ");
        for (int i = 0; i < strTempArray.length; i++) {
            strTempArray[i] = reverseString(strTempArray[i]);
        }
        String strNew = joinStrings(strTempArray, " ");
        for (int i = index + 1; i < str.length(); i++) {
            strNew += " ";
        }
        return strNew;
    }

    public static char[] getCharEntries(String str) {
        String strUnique = "";
        int[] intResult = new int[str.length()];
        for (int i = 0; i < str.length(); i++) {
            char letter = str.charAt(i);
            int indexOfLetter = strUnique.indexOf(letter);
            if (letter != ' ') {
                if (indexOfLetter == -1)
                    strUnique += letter;
                else
                    intResult[indexOfLetter] += 1;
            }
        }
        char[] charResult = strUnique.toCharArray();
        for (int i = 0; i < charResult.length - 1; i++) {
            for (int j = charResult.length - 1; j > i; j--) {
                if ((intResult[j] > intResult[j - 1]) || (intResult[j] == intResult[j - 1] && (charResult[j] < charResult[j - 1]))) {
                    char cTmp = charResult[j];
                    charResult[j] = charResult[j - 1];
                    charResult[j - 1] = cTmp;
                    int iTmp = intResult[j];
                    intResult[j] = intResult[j - 1];
                    intResult[j - 1] = iTmp;
                }
            }
        }
        return charResult;
    }

    public static String[] sortArray(String[] array, boolean asc) {
        if (array.length <= 1) {
            return array;
        }
        String[] tmpArray = array.clone();
        int middle = tmpArray.length / 2;
        String[] leftArray = new String[middle];
        String[] rightArray = new String[tmpArray.length - middle];
        int right = 0;
        for (int i = 0; i < tmpArray.length; i++) {
            if (i < middle) {
                leftArray[i] = tmpArray[i];
            } else {
                rightArray[right++] = tmpArray[i];
            }
        }
        leftArray = sortArray(leftArray, asc);
        rightArray = sortArray(rightArray, asc);
        return merge(leftArray, rightArray, asc);
    }

    private static String[] merge(String[] leftArray, String[] rightArray, boolean asc) {
        String[] merged = new String[leftArray.length + rightArray.length];
        int left = 0;
        int right = 0;
        for (int i = 0; i < leftArray.length + rightArray.length; i++) {
            if (left < leftArray.length && right < rightArray.length) {
                if (asc) {
                    if (leftArray[left].compareTo(rightArray[right]) > 0 && right < rightArray.length)
                        merged[i] = rightArray[right++];
                    else merged[i] = leftArray[left++];
                } else if (rightArray[right].compareTo(leftArray[left]) > 0 && right < rightArray.length)
                    merged[i] = rightArray[right++];
                else merged[i] = leftArray[left++];
            } else {
                if (left < leftArray.length)
                    merged[i] = leftArray[left++];
                else
                    merged[i] = rightArray[right++];
            }
        }
        return merged;
    }
}

