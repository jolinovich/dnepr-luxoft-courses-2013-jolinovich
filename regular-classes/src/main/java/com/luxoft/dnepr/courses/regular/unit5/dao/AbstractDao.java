package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.*;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    public AbstractDao() {
    }

    @Override
    public E save(E e) throws UserAlreadyExist {
        if (e == null) {
            return e;
        }
        Long id = e.getId();
        if (id == null) {
            id = EntityStorage.getMaxId() + 1;
        }
        if (EntityStorage.getEntities().containsKey(id)) {
            throw new UserAlreadyExist("User already exists");
        }
        e.setId(id);
        EntityStorage.getEntities().put(id, e);
        return e;
    }

    @Override
    public E update(E e) throws UserNotFound {
        if (e == null) {
            return null;
        }
        Long id = e.getId();
        if (id == null || !EntityStorage.getEntities().containsKey(id)) {
            throw new UserNotFound("User not found");
        }
        EntityStorage.getEntities().put(id, e);
        return e;
    }

    @Override
    public E get(long id) {
        if (!EntityStorage.getEntities().containsKey(id)) {
            return null;
        }
        return (E) EntityStorage.getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        if (!EntityStorage.getEntities().containsKey(id)) {
            return false;
        }
        EntityStorage.getEntities().remove(id);
        return true;
    }
}