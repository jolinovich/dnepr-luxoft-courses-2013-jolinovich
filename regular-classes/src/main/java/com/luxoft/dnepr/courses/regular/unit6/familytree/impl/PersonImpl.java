package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

public class PersonImpl implements Person {
    private String name;
    private String ethnicity;
    private Person father;
    private Person mother;
    private Gender gender;
    private int age;

    public PersonImpl() {
    }

    public PersonImpl(String name, String ethnicity, Person father, Person mother, Gender gender, int age) {
        this.name = name;
        this.ethnicity = ethnicity;
        this.father = father;
        this.mother = mother;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEthnicity() {
        return ethnicity;
    }

    @Override
    public Person getFather() {
        return father;
    }

    @Override
    public Person getMother() {
        return mother;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        String s = "";
        if (name != null) {
            s += "\"name\":\"" + name + "\",";
        }
        if (ethnicity != null) {
            s += "\"ethnicity\":\"" + ethnicity + "\",";
        }
        if (father != null) {
            s += "\"father\":{" + father.toString() + "},";
        }
        if (mother != null) {
            s += "\"mother\":{" + mother.toString() + "},";
        }
        if (gender != null) {
            s += "\"gender\":\"" + gender.toString() + "\",";
        }
        if (age > 0) {
            s += "\"age\":" + age + ",";
        }
        if (s.endsWith(",")) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonImpl person = (PersonImpl) o;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (ethnicity != null ? !ethnicity.equals(person.ethnicity) : person.ethnicity != null) return false;
        if (father != null ? !father.equals(person.father) : person.father != null) return false;
        if (mother != null ? !mother.equals(person.mother) : person.mother != null) return false;
        if (gender != person.gender) return false;
        if (age != person.age) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (ethnicity != null ? ethnicity.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }
}
