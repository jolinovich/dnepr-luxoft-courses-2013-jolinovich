package com.luxoft.dnepr.courses.regular.unit6.familytree;

import java.io.Serializable;

public interface Person extends Serializable {

	String getName();
	String getEthnicity();
	Person getFather();
	Person getMother();
	Gender getGender();
	int getAge();

    void setName(String name);
    void setEthnicity(String ethnicity);
    void setFather(Person father);
    void setMother(Person mother);
    void setGender(Gender gender);
    void setAge(int age);
}
