package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private List<Product> compositeProduct = new ArrayList<>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    private CompositeProduct searchCompositeProduct(Product product) {
        CompositeProduct tempProduct;
        for (Product product1 : compositeProduct) {
            tempProduct = (CompositeProduct) product1;
            if (tempProduct.getFirst().equals(product)) {
                return tempProduct;
            }
        }
        return null;
    }

    public void append(Product product) {
        CompositeProduct product1 = searchCompositeProduct(product);
        if (product1 == null) {
            product1 = new CompositeProduct();
            compositeProduct.add(product1);
        }
        product1.add(product);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double price = 0;
        for (Product product : compositeProduct) {
            price += product.getPrice();
        }
        return price;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */

    public List<Product> getProducts() {
        Collections.sort(compositeProduct, new ProductComparator());
        return compositeProduct;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    private static class ProductComparator implements Comparator<Product> {
        @Override
        public int compare(Product product1, Product product2) {
            if (product2.getPrice() > product1.getPrice()) return 1;
            else if (product2.getPrice() < product1.getPrice()) return -1;
            else return 0;
        }
    }

}
