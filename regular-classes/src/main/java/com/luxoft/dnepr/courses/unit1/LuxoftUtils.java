package com.luxoft.dnepr.courses.unit1;

public final class LuxoftUtils {

    private static final String NOT_DECIMAL_ERROR_MESSAGE = "Not decimal";
    private static final String NOT_BINARY_ERROR_MESSAGE = "Not binary";
    private static final int MIN_MONTH = 1;
    private static final int MAX_MONTH = 12;

    private static final String UNKNOWN_MONTH_MESSAGE_EN = "Unknown Month";
    private static final String UNKNOWN_MONTH_MESSAGE_RU = "Неизвестный месяц";

    private static final String UNKNOWN_LANGUAGE_MESSAGE = "Unknown Language";

    private static final String[] ENGLISH_MONTHS = {"January", "February", "March", "April", "May", "June"
            , "July", "August", "September", "October", "November", "December"};
    private static final String[] RUSSIAN_MONTHS = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь"
            , "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

    private static final String ENGLISH_LANG = "en";
    private static final String RUSSIAN_LANG = "ru";

    /*
    * As a rule utility classes has private constructors and static methods.
    */
    private LuxoftUtils() { }

    /**
     *
     * @param monthOrder order of month in year
     * @param language
     * @return month name in <code>language</code> by its order
     */
    public static String getMonthName(int monthOrder, String language) {
        if (language == null || !ENGLISH_LANG.equals(language) && !RUSSIAN_LANG.equals(language)) {
            return UNKNOWN_LANGUAGE_MESSAGE;
        }
        if (monthOrder < MIN_MONTH || monthOrder > MAX_MONTH) {
            return ENGLISH_LANG.equals(language) ? UNKNOWN_MONTH_MESSAGE_EN : UNKNOWN_MONTH_MESSAGE_RU;
        }
        return ENGLISH_LANG.equals(language) ? ENGLISH_MONTHS[monthOrder - 1] : RUSSIAN_MONTHS[monthOrder - 1];
    }

    /**
     * Converts binary number to decimal
     * @param binaryNumber
     * @return decimal representation of input <code>binaryNumber</code>
     */
    public static String binaryToDecimal(String binaryNumber) {
        if (!isBinary(binaryNumber)) {
            return NOT_BINARY_ERROR_MESSAGE;
        }
        char[] symbols = binaryNumber.toCharArray();
        int decimal = 0;
        int length = symbols.length;
        for (int i = 0; i < length; i++) {
            decimal += Math.pow(2, i) * Character.getNumericValue(symbols[length - i - 1]);
        }
        return String.valueOf(decimal);
    }

    public static String decimalToBinary(String decimalNumber) {
        if (!isDecimal(decimalNumber)) {
            return NOT_DECIMAL_ERROR_MESSAGE;
        }
        StringBuilder result = new StringBuilder();
        int number = Integer.parseInt(decimalNumber);
        while (number > 0) {
            int rest = number / 2;
            result.insert(0, number - rest * 2);
            number = rest;
        }
        return result.toString();
    }

    private static boolean isBinary(String number) {
        return isNthSystemNumber(number, '0', '1');
    }

    private static boolean isDecimal(String number) {
        return isNthSystemNumber(number, '0', '9');
    }

    private static boolean isNthSystemNumber(String number, char lowerBound, char upperBound) {
        if (number == null || number.isEmpty()) {
            return false;
        }
        char[] symbols = number.toCharArray();
        for (char symbol : symbols) {
            if ((symbol < lowerBound || symbol > upperBound)) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param array integer array
     * @param asc
     * @return sorted array in ascending order, if asc=<code>true</code>, otherwise in descending order.
     */
    public static int[] sortArray(int[] array, boolean asc) {
        int[] result = array.clone();

        while (performSortIteration(result, asc)) {
        }

        return result;
    }

    private static boolean performSortIteration(int[] result, boolean asc) {
        boolean needOneMoreIter = false;
        for (int j = 1; j < result.length; j++) {
            if (needToSwap(result[j - 1], result[j], asc)) {
                int temp = result[j - 1];
                result[j - 1] = result[j];
                result[j] = temp;
                needOneMoreIter = true;
            }
        }
        return needOneMoreIter;
    }

    public static boolean needToSwap(int prev, int next, boolean asc) {
        return (prev > next) == asc;
    }

    /*public static String getMonthName(int monthOrder, String language) {
        if (language.equals("en"))
            switch (monthOrder) {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return "Unknown Month";
            }
        else if (language.equals("ru"))
            switch (monthOrder) {
                case 1:
                    return "Январь";
                case 2:
                    return "Февраль";
                case 3:
                    return "Март";
                case 4:
                    return "Апрель";
                case 5:
                    return "Май";
                case 6:
                    return "Июнь";
                case 7:
                    return "Июль";
                case 8:
                    return "Август";
                case 9:
                    return "Сентябрь";
                case 10:
                    return "Октябрь";
                case 11:
                    return "Ноябрь";
                case 12:
                    return "Декабрь";
                default:
                    return "Неизвестный месяц";
            }
        else
            return "Unknown Language";
    }*/

    /*public static String hello(String name) {
        if (name == null)
            return "I don't know you";
        return "Hi, " + name;

    }     */


}