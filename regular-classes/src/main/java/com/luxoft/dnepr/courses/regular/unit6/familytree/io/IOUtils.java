package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;

import java.io.*;

public class IOUtils {

    private IOUtils() {
    }

    public static FamilyTree load(String filename) throws Exception {
        try (FileInputStream fis = new FileInputStream(filename)) {
            return load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static FamilyTree load(InputStream is) throws Exception {
        FamilyTree familyTree = null;
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            familyTree = (FamilyTree) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return familyTree;
    }

    public static void save(String filename, FamilyTree familyTree) throws Exception {
        try (FileOutputStream fos = new FileOutputStream(filename)) {
            save(fos, familyTree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void save(OutputStream os, FamilyTree familyTree) throws Exception {
        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(familyTree);
            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
