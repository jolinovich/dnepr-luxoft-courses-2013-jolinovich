package com.luxoft.dnepr.courses.regular.unit14;

import java.util.HashSet;

public class Estimation {
    private static HashSet<String> knownWords = new HashSet<>();
    private static HashSet<String> unknownWords = new HashSet<>();

    public int estimatedWords(int total) {
        int totalSize = knownWords.size() + unknownWords.size();
        return total * (knownWords.size() + 1) / (totalSize + 1);
    }

    public void printResult(int total) {
        System.out.println("Your estimated vocabulary is " + estimatedWords(total) + " words");
    }

    public static void answer(String word, Boolean isKnown) {
        if (isKnown) {
            knownWords.add(word);
        } else {
            unknownWords.add(word);
        }
    }
}
