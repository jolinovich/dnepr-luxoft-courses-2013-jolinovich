package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

public class EntityStorage {
    private final static Map<Long, Entity> entities = new HashMap();

    private EntityStorage() {
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }

    public static Long getMaxId() {
        Long maxId = Long.MIN_VALUE;
        for (Long id : entities.keySet()) {
            if (id > maxId) {
                maxId = id;
            }
        }
        return maxId;
    }
}