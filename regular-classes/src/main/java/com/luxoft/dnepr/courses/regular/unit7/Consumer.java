package com.luxoft.dnepr.courses.regular.unit7;

import java.io.*;
import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    public final static File LASTFILE = new File("");
    private WordStorage wordStorage;
    private BlockingQueue<File> fileQueue;

    public Consumer(WordStorage wordStorage, BlockingQueue<File> fileQueue) {
        this.wordStorage = wordStorage;
        this.fileQueue = fileQueue;
    }

    @Override
    public void run() {
        boolean last = true;
        try {
            while (last) {
                File file = fileQueue.take();
                if (file == LASTFILE) {
                    fileQueue.put(LASTFILE);
                    last = false;
                } else {
                    readFile(file);
                }
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void readFile(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        BufferedReader input = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String read;
        while ((read = input.readLine()) != null) {
            sb.append(read).append(";");
        }
        fis.close();
        String[] worlds = sb.toString().split("(?U)[^0-9A-Za-zА-Яа-яЁё]");
        for (String s : worlds) {
            wordStorage.save(s);
        }
    }
}
