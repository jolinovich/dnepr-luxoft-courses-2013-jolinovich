package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    public final static File LASTFILE = new File("");
    private BlockingQueue<File> fileQueue;
    private final String rootFolder;

    public Producer(String rootFolder, BlockingQueue<File> fileQueue) {
        this.rootFolder = rootFolder;
        this.fileQueue = fileQueue;
    }

    @Override
    public void run() {
        findFiles(rootFolder);
        try {
            fileQueue.put(LASTFILE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void findFiles(String currentFolder) {
        File list[] = new File(currentFolder).listFiles();
        if (list == null) return;
        for (File file : list) {
            processFile(file);
        }
    }

    private void processFile(File file) {
        if (file.isDirectory()) {
            findFiles(file.getPath());
        } else {
            if (file.getName().endsWith(".txt")) {
                try {
                    fileQueue.put(file);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
