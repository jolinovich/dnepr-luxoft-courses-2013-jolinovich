package com.luxoft.dnepr.courses.regular.unit14;

import java.util.Scanner;

public class UserInterface implements Runnable {

    private static Estimation estimation = new Estimation();
    private static Vocabulary vocabulary = new Vocabulary();

    private static void doAction(String input, String word) {
        input = input.toLowerCase();
        if (input.equals("y")) {
            estimation.answer(word, true);
        } else if (input.equals("n")) {
            estimation.answer(word, false);
        } else if (input.equals("exit")) {
            estimation.printResult(vocabulary.getSize());
        } else if (input.equals("help")) {
            printHelp();
        } else {
            printError();
        }
    }

    public void run() {
        printHelp();
        Scanner scanner = new Scanner(System.in);
        String randomWord, userInput;
        do {
            randomWord = vocabulary.randomWord();
            printQuestion(randomWord);
            userInput = scanner.next();
            doAction(userInput, randomWord);
        } while (!userInput.toLowerCase().equals("exit") && !userInput.isEmpty());
    }

    public static void main(String[] args) {
        UserInterface userInterface = new UserInterface();
        userInterface.run();
    }

    private static void printHelp() {
        System.out.println("Linguistic analizator.");
        System.out.println("Enter \"y\" if you know the word.");
        System.out.println("Enter \"n\" if you don't know the word.\"");
        System.out.println("Enter \"help\" to get help.");
        System.out.println("Enter \"exit\" to estimate your vocabulary size.");
    }

    private static void printError() {
        System.out.println("Error while reading answer.");
        printHelp();
    }

    private static void printQuestion(String word) {
        System.out.println("Do you know translation of this word?:");
        System.out.println(word);
    }
}
