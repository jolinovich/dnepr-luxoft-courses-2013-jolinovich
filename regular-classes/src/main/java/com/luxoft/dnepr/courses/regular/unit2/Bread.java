package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct {//implements Product {
    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        this.weight = weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        if (!super.equals(obj)) return false;
        // object must be Bread at this point
        Bread bread = (Bread) obj;
        return bread.weight == weight;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 31 * hash + (int) weight;
        return hash;
    }
}
