package com.luxoft.dnepr.courses.regular.unit4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class EqualSet<E> implements Set<E> {
    private ArrayList<E> list;

    public EqualSet() {
        list = new ArrayList<>();
    }

    public EqualSet(Collection<? extends E> collection) {
        list = new ArrayList<>();
        if (collection != null) {
            addAll(collection);
        }
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        for (E e : list) {
            if (o == null && e == null) {
                return true;
            }
            if (e.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean add(E e) {
        return !list.contains(e) && list.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return c != null && list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (c == null) {
            return false;
        }
        boolean isAdded = false;
        for (E e : c) {
            if (add(e)) {
                isAdded = true;
            }
        }
        return isAdded;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return c != null && list.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return c != null && list.removeAll(c);
    }

    @Override
    public void clear() {
        list.clear();
    }
}