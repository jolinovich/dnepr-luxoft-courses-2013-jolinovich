package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {
    @Test
    public void testGetMonthName() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        Assert.assertEquals("Декабрь", LuxoftUtils.getMonthName(12, "ru"));
        Assert.assertEquals("Unknown Language",LuxoftUtils.getMonthName(5, "hindi"));
        Assert.assertEquals("Unknown Language",LuxoftUtils.getMonthName(13, "hindi"));
        Assert.assertEquals( "Неизвестный месяц", LuxoftUtils.getMonthName(-5, "ru"));
        //Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(0, null));
    }

    /*public void testHello() {
        Assert.assertEquals("Hi, Julia",LuxoftUtils.hello("Julia"));
    }

    public void testHelloError() {
        Assert.assertEquals("I don't know you", LuxoftUtils.hello(null));
    } */
}
