package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.AbstractDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AbstractDaoTest {
    private AbstractDao<Redis> redisDao = new RedisDaoImpl();
    private AbstractDao<Employee> employeeDao = new EmployeeDaoImpl();

    @Before
    public void init() throws UserAlreadyExist {
        EntityStorage.getEntities().clear();
        employeeDao.save(new Employee(1L, 1000));
        employeeDao.save(new Employee(2L, 2000));
        employeeDao.save(new Employee(3L, 1500));
        redisDao.save(new Redis(11L, 150));
        redisDao.save(new Redis(12L, 200));
        redisDao.save(new Redis(13L, 180));
    }

    @Test
    public void testSaveGet() throws UserAlreadyExist {
        try {
            employeeDao.save(new Employee(3L, 3000));
        } catch (UserAlreadyExist e) {
            Assert.assertEquals(e.getMessage(), "User already exists");
        }
        try {
            redisDao.save(new Redis(13L, 300));
        } catch (UserAlreadyExist e) {
            Assert.assertEquals(e.getMessage(), "User already exists");
        }
        employeeDao.save(new Employee(4L, 2500));
        redisDao.save(new Redis(14L, 250));
        Assert.assertEquals(8, EntityStorage.getEntities().size());
        Assert.assertEquals(null, employeeDao.save(null));
        Assert.assertEquals(8, EntityStorage.getEntities().size());
        Employee employee = new Employee();
        employee.setSalary(4000);
        employeeDao.save(employee);
        Assert.assertEquals(9, EntityStorage.getEntities().size());
        Assert.assertEquals(4000, employeeDao.get(15L).getSalary());
    }

    @Test
    public void testUpdateGet() throws UserNotFound {
        Assert.assertEquals(2000, employeeDao.get(2L).getSalary());
        Assert.assertEquals(150, redisDao.get(11L).getWeight());
        employeeDao.update(new Employee(2L, 2500));
        redisDao.update(new Redis(11L, 250));
        try {
            employeeDao.update(new Employee(4L, 3000));
        } catch (UserNotFound e) {
            Assert.assertEquals(e.getMessage(), "User not found");
        }
        try {
            redisDao.update(new Redis(14L, 300));
        } catch (UserNotFound e) {
            Assert.assertEquals(e.getMessage(), "User not found");
        }
        Assert.assertEquals(2500, employeeDao.get(2L).getSalary());
        Assert.assertEquals(250, redisDao.get(11L).getWeight());
        Assert.assertEquals(null, employeeDao.update(null));
    }

    @Test
    public void testDelete() throws Exception {
        Assert.assertTrue(redisDao.delete(11L));
        Assert.assertFalse(redisDao.delete(11L));
        Assert.assertTrue(employeeDao.delete(1L));
        Assert.assertTrue(employeeDao.delete(12L));
        Assert.assertEquals(3, EntityStorage.getEntities().size());
    }
}
