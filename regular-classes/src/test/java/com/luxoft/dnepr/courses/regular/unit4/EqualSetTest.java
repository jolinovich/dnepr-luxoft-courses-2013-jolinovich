package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class EqualSetTest {
    private EqualSet<String> list = new EqualSet<String>();
    private ArrayList collection = new ArrayList<String>();

    @Before
    public void Init() {
        list.add("one");
        list.add("two");
        list.add("four");
        collection.add("one");
        collection.add("two");
        collection.add("three");
    }

    @Test
    public void testConstructor() {
        EqualSet<Integer> set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8}));
        Assert.assertEquals(8, set.size());

        set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 1, 1, 2, 2}));
        Assert.assertEquals(2, set.size());
    }

    @Test
    public void testConstructorWithNulls() {
        EqualSet<Integer> set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, null}));
        Assert.assertEquals(9, set.size());

        set = new EqualSet(Arrays.asList(new Integer[]{1, 2, 1, 1, 2, 2, null, null, null, null}));
        Assert.assertEquals(3, set.size());
    }

    @Test
    public void testConstructor1() {
        EqualSet<String> list2 = new EqualSet<String>(list);
        Assert.assertEquals(3, list2.size());
        EqualSet<String> list3 = new EqualSet<String>(null);
        Assert.assertEquals(0, list3.size());
    }

    @Test
    public void testIsEmpty() {
        list.clear();
        Assert.assertTrue(list.isEmpty());
        list.add("one");
        Assert.assertFalse(list.isEmpty());
    }

    @Test
    public void testIterator() {
        list.add(null);
        Iterator<String> iter = list.iterator();
        Assert.assertEquals(iter.next(), "one");
        Assert.assertEquals(iter.next(), "two");
        Assert.assertEquals(iter.next(), "four");
        Assert.assertEquals(iter.next(), null);
        int i = 0;
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            Assert.assertTrue(list.contains(it.next()));
            i++;
        }
        Assert.assertEquals(i, list.size());
    }

    @Test
    public void testToArray() {
        Assert.assertEquals(list.size(), list.toArray().length);
        String[] strArray = {"one", "two", "four"};
        Assert.assertArrayEquals(strArray, list.toArray());
        list.add (null);
        Assert.assertEquals(list.size(), list.toArray().length);
        String[] strArrayNull = {"one", "two", "four", null};
        Assert.assertArrayEquals(strArrayNull,list.toArray());
        String[] strArray1 = new String [4];
        Assert.assertArrayEquals(strArray1, list.toArray(strArray1));
    }

    @Test
    public void testAdd() {
        list.add("four");
        Assert.assertTrue(list.add("five"));
        list.add(null);
        list.add(null);
        Assert.assertFalse(list.add(null));
        list.add(null);
        Assert.assertEquals(5, list.size());
    }

    @Test
    public void testAddAll() {
        Assert.assertTrue(list.addAll(collection));
        Assert.assertEquals(4, list.size());
        Assert.assertFalse(list.addAll(collection));
        Assert.assertFalse(list.addAll(null));
    }

    @Test
    public void testContains() {
        Assert.assertTrue(list.contains("one"));
        Assert.assertFalse(list.contains("three"));
        Assert.assertFalse(list.contains(null));
        list.add(null);
        Assert.assertTrue(list.contains(null));
    }

    @Test
    public void testContainsAll() {
        Assert.assertFalse(list.containsAll(collection));
        list.add("three");
        Assert.assertTrue(list.containsAll(collection));
        collection.add(null);
        Assert.assertFalse(list.containsAll(collection));
        list.add(null);
        Assert.assertTrue(list.containsAll(collection));
        Assert.assertFalse(list.containsAll(null));
    }

    @Test
    public void testRemove() {
        Assert.assertTrue(list.remove("one"));
        Assert.assertEquals(2, list.size());
        Assert.assertFalse(list.remove("one"));
        Assert.assertEquals(2, list.size());
        Assert.assertFalse(list.remove(null));
        list.add(null);
        Assert.assertEquals(3, list.size());
        Assert.assertTrue(list.remove(null));
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void testRemoveAll() {
        Assert.assertTrue(list.removeAll(collection));
        Assert.assertEquals(1, list.size());
        Assert.assertFalse(list.removeAll(collection));
        Assert.assertFalse(list.contains("one"));
        Assert.assertTrue(list.contains("four"));
        Assert.assertFalse(list.removeAll(null));
    }

    @Test
    public void testRetainAll() {
        Assert.assertTrue(list.retainAll(collection));
        Assert.assertEquals(2, list.size());
        Assert.assertFalse(list.retainAll(collection));
        Assert.assertFalse(list.contains("four"));
        Assert.assertTrue(list.contains("one"));
        Assert.assertFalse(list.retainAll(null));
    }

    @Test
    public void testClear() {
        list.clear();
        Assert.assertEquals(0, list.size());
    }
}