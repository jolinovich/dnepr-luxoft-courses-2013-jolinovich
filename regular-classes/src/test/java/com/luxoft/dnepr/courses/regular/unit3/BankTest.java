package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class BankTest {

    private static final String CURRENT_JAVA_VERSION = System.getProperty("java.version");

    @Test
    public void testExpectedJavaVersionFailure() {
        String expectedJavaVersion = "something_really_wrong";
        try {
            new Bank(expectedJavaVersion);
            Assert.fail("IllegalJavaVersionError must be thrown on something_really_wrong java version");
        } catch (IllegalJavaVersionError error) {
            Assert.assertEquals(expectedJavaVersion, error.getExpectedJavaVersion());
            Assert.assertEquals(CURRENT_JAVA_VERSION, error.getActualJavaVersion());
        }
    }

    @Test
    public void testExpectedJavaVersionOK() {
        new Bank(CURRENT_JAVA_VERSION);
    }

    @Test
    public void testMakeMoneyTransactionLimitExceeded() throws Exception {
        try {
            Bank bank = new Bank("1.7.0_40");
            Wallet fromWallet = new Wallet(1L, new BigDecimal(100.75), WalletStatus.ACTIVE, new BigDecimal(1000));
            Wallet toWallet = new Wallet(2L, new BigDecimal(200.50), WalletStatus.ACTIVE, new BigDecimal(255.50));
            User fromUser = new User(1, "Julia", fromWallet);
            User toUser = new User(2, "Jane", toWallet);
            bank.getUsers().put(fromUser.getId(), fromUser);
            bank.getUsers().put(toUser.getId(), toUser);
            bank.makeMoneyTransaction(1L, 2L, new BigDecimal(100.50));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'Jane' wallet limit exceeded (200.50 + 100.50 > 255.50)");
        }
    }

    @Test
    public void testMakeMoneyTransactionNoUserFound() throws Exception {
        try {
            Bank bank = new Bank("1.7.0_40");
            Wallet fromWallet = new Wallet(1L, new BigDecimal(100.50), WalletStatus.ACTIVE, new BigDecimal(1000));
            Wallet toWallet = new Wallet(2L, new BigDecimal(200.75), WalletStatus.ACTIVE, new BigDecimal(255.55));
            User fromUser = new User(1, "From User", fromWallet);
            User toUser = new User(2, "To User", toWallet);
            bank.getUsers().put(fromUser.getId(), fromUser);
            bank.makeMoneyTransaction(1L, 2L, new BigDecimal(100));
        } catch (NoUserFoundException e) {
            Assert.assertEquals(e.getMessage(), "User with id 2 not found");
        }
    }

    @Test
    public void testMakeMoneyTransactionInsufficientWalletAmount() throws Exception {
        try {
            Bank bank = new Bank("1.7.0_40");
            Wallet fromWallet = new Wallet(1L, new BigDecimal(100.50), WalletStatus.ACTIVE, new BigDecimal(1000));
            Wallet toWallet = new Wallet(2L, new BigDecimal(200.75), WalletStatus.ACTIVE, new BigDecimal(255.55));
            User fromUser = new User(1, "Julia", fromWallet);
            User toUser = new User(2, "Jane", toWallet);
            bank.getUsers().put(fromUser.getId(), fromUser);
            bank.getUsers().put(toUser.getId(), toUser);
            bank.makeMoneyTransaction(1L, 2L, new BigDecimal(150.50));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'Julia' has insufficient funds (100.50 < 150.50)");
        }
    }

    @Test
    public void testMakeMoneyTransactionWalletIsBlocked() throws Exception {
        try {
            Bank bank = new Bank("1.7.0_40");
            Wallet fromWallet = new Wallet(1L, new BigDecimal(100.50), WalletStatus.BLOCKED, new BigDecimal(1000));
            Wallet toWallet = new Wallet(2L, new BigDecimal(200.75), WalletStatus.ACTIVE, new BigDecimal(255.55));
            User fromUser = new User(1, "Julia", fromWallet);
            User toUser = new User(2, "Jane", toWallet);
            bank.getUsers().put(fromUser.getId(), fromUser);
            bank.getUsers().put(toUser.getId(), toUser);
            bank.makeMoneyTransaction(1L, 2L, new BigDecimal(150.50));
        } catch (TransactionException e) {
            Assert.assertEquals(e.getMessage(), "User 'Julia' wallet is blocked");
        }
    }

    @Test
    public void testBank() throws Exception {
        try {
            Bank bank = new Bank("1.6.0");
        } catch (IllegalJavaVersionError e) {
            Assert.assertEquals(e.getMessage(), "Illegal Java version. Actual: 1.7.0_40");
        }
    }
}