package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

public class IOUtilsTest {
    private Person grandfather;
    private Person grandmother;
    private Person father;
    private Person mother;
    private Person person;

    @Before
    public void init() throws Exception {
        grandfather = new PersonImpl("grandpapa", "ukrainian", null, null, Gender.MALE, 70);
        grandmother = new PersonImpl("grandmama", "ukrainian", null, null, Gender.FEMALE, 65);
        father = new PersonImpl("papa", "ukrainian", grandfather, grandmother, Gender.MALE, 45);
        mother = new PersonImpl("mama", "ukrainian", null, null, Gender.FEMALE, 40);
        person = new PersonImpl("children", "ukrainian", father, mother, Gender.MALE, 20);
    }

    @Test
    public void testSave() throws Exception {
        FamilyTree family = FamilyTreeImpl.create(person);
        IOUtils.save("family.json", family);
        FamilyTree fatherFamily = FamilyTreeImpl.create(father);
        IOUtils.save("father.json", fatherFamily);
        FamilyTree grandFatherFamily = FamilyTreeImpl.create(grandfather);
        IOUtils.save("grandfather.json", grandFatherFamily);
    }

    @Test
    public void testLoad() throws Exception {
        FamilyTree grandfatherFamily = IOUtils.load("grandfather.json");
        Person grandfather1 = grandfatherFamily.getRoot();
        Assert.assertEquals(grandfather1, grandfather);

        FamilyTree fatherFamily = IOUtils.load("father.json");
        Person father1 = fatherFamily.getRoot();
        Assert.assertEquals(father, father1);

        FamilyTree family = IOUtils.load("family.json");
        Person person1 = family.getRoot();
        Assert.assertEquals(person, person1);
    }
}
