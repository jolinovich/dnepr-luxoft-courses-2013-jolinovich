package com.luxoft.dnepr.courses.unit2;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {
    @Test
    public void testWordAverageLength() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), 0.0001);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat "), 0.0001);
        Assert.assertEquals(2.5, LuxoftUtils.wordAverageLength("I have a 3cats"), 0.0001);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("  I    have a   cat   "), 0.0001);
        Assert.assertEquals(0, LuxoftUtils.wordAverageLength(""), 0.0001);
    }

    /**@Test
    public void testReverseString() {
        Assert.assertEquals("cba", LuxoftUtils.reverseString("abc"));
        Assert.assertEquals("", LuxoftUtils.reverseString(""));
    } */

    @Test
    public void testReverseWords() {
        Assert.assertEquals("cba trf gr", LuxoftUtils.reverseWords("abc frt rg"));
        Assert.assertEquals("cba  trf   gr", LuxoftUtils.reverseWords("abc  frt   rg"));
        Assert.assertEquals("  cba  trf   gr", LuxoftUtils.reverseWords("  abc  frt   rg"));
        Assert.assertEquals("  cba  trf   gr  ", LuxoftUtils.reverseWords("  abc  frt   rg  "));
        Assert.assertEquals("", LuxoftUtils.reverseWords(""));
    }

    @Test
    public void testGetCharEntries() {
        char[] expectedArray = {'a', 'I', 'c', 'e', 'h', 't', 'v'};
        Assert.assertArrayEquals(expectedArray, LuxoftUtils.getCharEntries("I have a cat"));
        Assert.assertArrayEquals(expectedArray, LuxoftUtils.getCharEntries("a cat I have"));
        char[] expectedArray1 = {'a', 'e', 'c', 'h', 't', 'v', 'w'};
        Assert.assertArrayEquals(expectedArray1, LuxoftUtils.getCharEntries("we have a cat"));
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("aaaAAA zzzSSS"));
        Assert.assertArrayEquals(new char[]{}, LuxoftUtils.getCharEntries(""));
    }

    @Test
    public void testSortArray() {
        String[] testArray = {"I", "really", "want", "to", "sleep"};
        String[] expectedArrayTrue = {"I", "really", "sleep", "to", "want"};
        String[] expectedArrayFalse = {"want", "to", "sleep", "really", "I"};
        Assert.assertArrayEquals(expectedArrayTrue, LuxoftUtils.sortArray(testArray, true));
        Assert.assertArrayEquals(expectedArrayFalse, LuxoftUtils.sortArray(testArray, false));
        String[] testArray1 = {"we", "have", "a", "cat"};
        String[] expectedArrayTrue1 = {"a", "cat", "have", "we"};
        String[] expectedArrayFalse1 = {"we", "have", "cat", "a"};
        Assert.assertArrayEquals(expectedArrayTrue1, LuxoftUtils.sortArray(testArray1, true));
        Assert.assertArrayEquals(expectedArrayFalse1, LuxoftUtils.sortArray(testArray1, false));
    }
}
