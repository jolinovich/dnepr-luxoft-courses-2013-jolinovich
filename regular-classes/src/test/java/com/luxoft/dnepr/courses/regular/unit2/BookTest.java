package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        Assert.assertEquals(cloned.getCode(), book.getCode());
        Assert.assertEquals(cloned.getName(), book.getName());
        Assert.assertEquals(cloned.getPrice(), book.getPrice(), 0);
        Assert.assertEquals(cloned.getPublicationDate(), book.getPublicationDate());
        Assert.assertFalse(cloned.getPublicationDate() == book.getPublicationDate());
    }

    @Test
    public void testEquals() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 150, new GregorianCalendar(2006, 0, 1).getTime());
        Assert.assertTrue(book1.equals(book2));
        Assert.assertTrue(book2.equals(book1));
        Book book3 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2007, 0, 1).getTime());
        Assert.assertFalse(book1.equals(book3));
        Book book4 = productFactory.createBook("code", "Thinking in Java ", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Assert.assertFalse(book1.equals(book4));
    }

    @Test
    public void testHashCode() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 150, new GregorianCalendar(2006, 0, 1).getTime());
        Assert.assertEquals(book1.hashCode(), book2.hashCode());
        //Assert.assertSame(book1.hashCode(), book2.hashCode());
        Book book3 = productFactory.createBook("code", "Thinking in Java ", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Assert.assertNotSame(book1.hashCode(), book3.hashCode());
        Book book4 = productFactory.createBook("code", "Thinking in Java ", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Assert.assertNotSame(book1.hashCode(), book4.hashCode());
    }
}
