package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        if (input == null) {
            throw new CompilationException("Input error!");
        }
        try {
            addOperand(arrayOfSigns(input), result);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            throw new CompilationException("compilation error");
        }
        return result.toByteArray();
    }

    private static String[] arrayOfSigns(String input) {
        input = input.trim();
        while (input.contains("  ")) {
            input = input.replace("  ", " ");
        }
        if (isValidExpr(input)) {
            input = input.replace(" ", "");
            input = input.replace("+", " + ");
            input = input.replace("-", " - ");
            input = input.replace("*", " * ");
            input = input.replace("/", " / ");
            return input.split(" ");
        }
        else throw  new CompilationException("Wrong expression");
        //return null;
    }

    private static boolean isValidNumber(String input) {
        String numSign = "0123456789.";
        int dotCount = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '.') {
                dotCount++;
            }
            if (dotCount > 1) {
                return false;
            }
            if (numSign.indexOf(input.charAt(i)) < 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean isValidExpr(String input) {
        String arifmSign = "+-*/";
        String numSign = "0123456789.";
        if (arifmSign.indexOf(input.charAt(0)) >= 0 || arifmSign.indexOf(input.charAt(input.length() - 1)) >= 0) {
            return false;
        }
        for (int i = 0; i < input.length(); i++) {
            if (i > 0 && i < input.length() - 1) {
                if (input.charAt(i) == ' ' && numSign.indexOf(input.charAt(i - 1)) >= 0 && numSign.indexOf(input.charAt(i + 1)) >= 0) {
                    return false;
                }
                if (input.charAt(i) == ' ' && arifmSign.indexOf(input.charAt(i - 1)) >= 0 && arifmSign.indexOf(input.charAt(i + 1)) >= 0) {
                    return false;
                }
            }
            if (i > 0 && arifmSign.indexOf(input.charAt(i - 1)) >= 0 && arifmSign.indexOf(input.charAt(i)) >= 0) {
                return false;
            }
            if (numSign.indexOf(input.charAt(i)) < 0 && arifmSign.indexOf(input.charAt(i)) < 0 && input.charAt(i) != ' ') {
                return false;
            }
        }
        return true;
    }

    //public static void addOperand(double first, double second, char sign, ByteArrayOutputStream result) {
    public static void addOperand(String[] inputArray, ByteArrayOutputStream result) {
        double first;
        double second = 0;
        if (isValidNumber(inputArray[0])) {
            first = Double.parseDouble(inputArray[0]);
        } else throw new CompilationException("Wrong number");
        if (isValidNumber(inputArray[0])) {
            second = Double.parseDouble(inputArray[2]);
        } else throw new CompilationException("Wrong number");
        char sign = inputArray[1].charAt(0);
        addCommand(result, VirtualMachine.PUSH, first);
        addCommand(result, VirtualMachine.PUSH, second);
        addOperation(sign, result);
        addCommand(result, VirtualMachine.PRINT);
    }

    public static void addOperation(char sign, ByteArrayOutputStream result) {
        if (sign == '-' || sign == '/') {
            addCommand(result, VirtualMachine.SWAP);
        }
        switch (sign) {
            case '+':
                addCommand(result, VirtualMachine.ADD);
                return;
            case '-':
                addCommand(result, VirtualMachine.SUB);
                return;
            case '*':
                addCommand(result, VirtualMachine.MUL);
                return;
            case '/':
                addCommand(result, VirtualMachine.DIV);
        }
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
