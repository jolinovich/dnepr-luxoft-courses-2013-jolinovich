package com.luxoft.dnepr.courses.compiler;

public enum TokenValue {
    PLUS('+'),
    MINUS('-'),
    MUL('*'),
    DIV('/'),
    LP('('),
    RP(')');

    private final char sign;

    TokenValue(char sign) {
        this.sign = sign;
    }

    public char getSign() {
        return sign;
    }
}
