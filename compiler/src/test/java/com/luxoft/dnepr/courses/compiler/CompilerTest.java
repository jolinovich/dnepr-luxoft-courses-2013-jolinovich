package com.luxoft.dnepr.courses.compiler;

import org.junit.Assert;
import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

   /* @Test
    public void testIsValidExpr() {
        Assert.assertEquals(false,Compiler.isValidExpr("2 2+2"));
        Assert.assertEquals(true,Compiler.isValidExpr("2+2"));
        Assert.assertEquals(true,Compiler.isValidExpr("2.+2"));
        Assert.assertEquals(false,Compiler.isValidExpr("2+ *2"));
        Assert.assertEquals(false,Compiler.isValidExpr("2+*2"));
        Assert.assertEquals(false,Compiler.isValidExpr("2a+ 2"));
        Assert.assertEquals(false,Compiler.isValidExpr("+ 2"));
        Assert.assertEquals(false,Compiler.isValidExpr("2-"));
    }*/

   /* @Test
    public void testIsValidExpr() {
        String[] expectedArray = {"2", "+", "3.12"};
        Assert.assertArrayEquals(expectedArray, Compiler.arrayOfSigns("2  +3.12"));
    }*/

    /*@Test
    public void testIsValidNumber() {
        Assert.assertEquals(false,Compiler.isValidNumber("2.."));
        Assert.assertEquals(true,Compiler.isValidNumber("2.5"));
        Assert.assertEquals(true,Compiler.isValidNumber("2."));
        Assert.assertEquals(true,Compiler.isValidNumber(".2"));
        Assert.assertEquals(false,Compiler.isValidNumber("2.4."));
        Assert.assertEquals(false,Compiler.isValidNumber("2..3"));
        Assert.assertEquals(false,Compiler.isValidNumber("..4"));
    }*/

    @Test
    public void testSimple() {
        assertCompiled(4, " 2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(.6, " 2  * .3 ");
        assertCompiled(6, " 2  * 3. ");
        assertCompiled(4.5, "  9 /2 ");
    }

    /*@Test
    public void testComplex() {
        assertCompiled(12, "  (2 + 2 ) * 3 ");
        assertCompiled(8.5, "  2.5 + 2 * 3 ");
        assertCompiled(8.5, "  2 *3 + 2.5");
    }*/

    @Test
    public void testExpectedException() {
        try {
            Compiler.compile(null);
            assert false;
        } catch (CompilationException e) {
            assert true;
        }
        try {
            Compiler.compile("2..1+2");
            assert false;
        } catch (CompilationException e) {
            assert true;
        }
        try {
            Compiler.compile("2*+2");
            assert false;
        } catch (CompilationException e) {
            assert true;
        }
        try {
            Compiler.compile("2. 1+2");
            assert false;
        } catch (CompilationException e) {
            assert true;
        }
    }
}
